#ifndef _FCITX5_UI2TXT_UI2TXT_H_
#define _FCITX5_UI2TXT_UI2TXT_H_

#include <fcitx/addonfactory.h>
#include <fcitx/addoninstance.h>
#include <fcitx/addonmanager.h>
#include <fcitx/inputmethodengine.h>
#include <fcitx/instance.h>

class UI2Txt : public fcitx::AddonInstance {
public:
  UI2Txt(fcitx::Instance *instance);
  ~UI2Txt();

  fcitx::Instance *instance() { return instance_; }

private:
  fcitx::Instance *instance_;
  std::vector<std::unique_ptr<fcitx::HandlerTableEntry<fcitx::EventHandler>>>
      eventHandlers_;
};

class UI2TxtFactory : public fcitx::AddonFactory {
  fcitx::AddonInstance *create(fcitx::AddonManager *manager) override {
    return new UI2Txt(manager->instance());
  }
};

#endif // _FCITX5_UI2TXT_UI2TXT_H_
