#include "ui2txt.h"
#include <cstring>
#include <fcitx/event.h>
#include <fcitx/inputpanel.h>
#include <fcitx/instance.h>
#include <fstream>

UI2Txt::UI2Txt(fcitx::Instance *instance) : instance_(instance) {

  eventHandlers_.emplace_back(instance->watchEvent(
      fcitx::EventType::InputContextCursorRectChanged,
      fcitx::EventWatcherPhase::PreInputMethod, [](fcitx::Event &event) {
        auto &cursorRectChangedEvent =
            static_cast<fcitx::CursorRectChangedEvent &>(event);
        auto *inputContext = cursorRectChangedEvent.inputContext();
        auto &area = inputContext->cursorRect();
        std::ofstream ime_pos_file("/tmp/iimmeexxyy",
                                   std::ios::out | std::ios::trunc);
        ime_pos_file << area.left() << " " << area.top() << " " << area.width()
                     << " " << area.height();

        ime_pos_file.close();
      }));

  eventHandlers_.emplace_back(instance->watchEvent(
      fcitx::EventType::InputContextUpdateUI,
      fcitx::EventWatcherPhase::PostInputMethod, [this](fcitx::Event &event) {
        auto &updateUIevent =
            static_cast<fcitx::InputContextUpdateUIEvent &>(event);
        auto *inputContext = updateUIevent.inputContext();
        if (fcitx::stringutils::startsWith(inputContext->display(),
                                           "wayland:") &&
            strcmp(inputContext->frontend(), "dbus") == 0 &&
            !inputContext->capabilityFlags().test(
                fcitx::CapabilityFlag::ClientSideInputPanel)) {
          if (updateUIevent.component() ==
              fcitx::UserInterfaceComponent::InputPanel) {

            auto *instance = instance_;
            auto &inputPanel = inputContext->inputPanel();

            auto preedit =
                instance->outputFilter(inputContext, inputPanel.preedit());
            auto auxUp =
                instance->outputFilter(inputContext, inputPanel.auxUp());

            std::string imestr;
            imestr.reserve(512u);

            if (preedit.cursor() >= 0 &&
                static_cast<size_t>(preedit.cursor()) <= preedit.textLength()) {

              imestr.append(preedit.toString());
              imestr.push_back('\n');
              for (int i = 1; i <= preedit.cursor(); i++)
                imestr.push_back(' ');

              imestr.push_back('^');
              imestr.push_back('\n');
            } else {
              imestr.append(auxUp.toString());
            }

            if (auto candidateList = inputPanel.candidateList()) {
              // Count non-placeholder candidates.
              int count = 0;

              for (int i = 0, e = candidateList->size(); i < e; i++) {
                const auto &candidate = candidateList->candidate(i);
                if (candidate.isPlaceHolder()) {
                  continue;
                }
                count++;
              }

              int candidateIndex_ = -1;
              int localIndex = 0;
              for (int i = 0, e = candidateList->size(); i < e; i++) {
                const auto &candidate = candidateList->candidate(i);
                // Skip placeholder.
                if (candidate.isPlaceHolder()) {
                  continue;
                }

                if (i == candidateList->cursorIndex()) {
                  candidateIndex_ = localIndex;
                }

                fcitx::Text labelText = candidate.hasCustomLabel()
                                            ? candidate.customLabel()
                                            : candidateList->label(i);

                labelText = instance->outputFilter(inputContext, labelText);

                auto candidateText =
                    instance->outputFilter(inputContext, candidate.text());

                localIndex++;
                if (i == candidateIndex_) {
                  imestr.push_back('>');
                } else {
                  imestr.push_back(' ');
                }
                imestr.append(labelText.toString());
                imestr.push_back(' ');

                imestr.append(candidateText.toString());
                imestr.push_back('\n');
              }
            }
            std::ofstream imefile("/tmp/iimmee", std::ios::out | std::ios::trunc);

            imefile << imestr;
            imefile.close();
          }
        }
      }));
}

UI2Txt::~UI2Txt() {}
FCITX_ADDON_FACTORY(UI2TxtFactory);
