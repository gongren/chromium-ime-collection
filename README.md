#  一些在Chromium下模拟显示Fcitx5输入框的办法

目前在sway环境下，使用了GTK4的Chromium对输入法的支持存在缺陷，表现为：无法显示输入框，但是可以正常输入汉字。在此问题得到解决之前，本库可以提供一些缓解的办法，主要思路是：使用独立程序为Chromium显示一个模拟的输入框，结合键盘可以较为方便地输入汉字。

## 使用官方的 fcitx5-wayland-launcher

在sway的配置文件中加入下面这一行，fcitx5可以使用Xwayland显示输入框。当Chromium窗口不处于独占屏幕状态时，输入框的位置会有偏差。

```
exec --no-startup-id /usr/lib/fcitx5-wayland-launcher
```

## fcitx5-ui2txt

将UI内容写入到文本文件中，以便下述各程序读取。

## ime-popup

使用终端窗口显示输入框

![2](https://gitlab.com/gongren/chromium-ime-collection/-/wikis/uploads/a5e9fc5c5ab67316c005132017877335/2.png)

在sway环境下，需要将chromium设置为非浮动窗口，这样浮动的输入框才能显示在浏览器上方。

### 使用方法

```sh
cd ime-popup

cargo build

export P_APP_SIZE="170 270"
export P_APP_ID=_pop
alacritty --class _pop  -e target/debug/ime-popup
```

### 环境变量
`P_APP_SIZE="170 270"`用来设置输入框大小

`P_APP_ID=_pop` 用来设置输入框终端窗口的ID

## fcitx5-proxy

在浏览器网页内显示输入框

![1](https://gitlab.com/gongren/chromium-ime-collection/-/wikis/uploads/f8853320bc9e32558cc061b874b2c97d/1.png)


### 使用方法

```sh
cd fcitx5-proxy
cargo build

target/debug/fcitx5-proxy
```

复制`client/tool.js`文件内容；在chromium中添加任意一个网页书签，编辑该书签，将网址内容填写
`javascript:`，并在冒号后面粘贴`tool.js`文件的内容，之后保存；切换到要使用输入法的标签页，点击该书签按钮。


## py-ime-popup

使用GTK3显示输入框

![3](https://gitlab.com/gongren/chromium-ime-collection/-/wikis/uploads/b9e08ec03132269bf272388f7e2dbadf/3.png)

运行依赖：
- watchdog
- GtkLayerShell
- Gtk3
- python-gobject
- i3ipc

### 使用方法

```sh
cd py-ime-popup
python main.py
```

## 支持程度

在sway环境下，上述程序对Chromium显示界面的支持程度。

注意，上述程序只是被动地显示模拟UI，所以鼠标操作没有作用。

下表中“UI”包括Chromium的地址栏、搜索框、开发者工具等界面。

| 程序\模式    | 全屏（网页内） | 全屏（UI） | 非全屏（网页内） | 非全屏（UI） |
| ------------ | :------------: | :--------: | :--------------: | :----------: |
| fcitx5-proxy |       ✔        |     ✔+     |        ✔         |      ✔+      |
| ime-popup    |                |            |        ✔*        |      ✔*      |
| py-ime-popup |       ✔        |     ✔      |        ✔         |      ✔       |

*+输入框显示在网页内，不能跟随光标*

**需要Chromium处于贴靠状态，否则输入框会显示在已获得焦点的Chromium窗口后面*



## 参考链接

https://github.com/fcitx/fcitx5/blob/5.0.23/src/ui/classic/classicui.cpp#L279

https://fcitx-im.org/wiki/Using_Fcitx_5_on_Wayland

https://fcitx-im.org/wiki/Develop_an_simple_input_method#Understand_the_file_structure_for_a_Fcitx_shared_library_addon

https://fcitx-im.org/wiki/Theme_Customization#Kimpanel

https://www.csslayer.info/wordpress/fcitx-dev/chrome-state-of-input-method-on-wayland/

https://www.csslayer.info/wordpress/fcitx-dev/client-side-input-panel-for-fcitx-5/