(function() {
    let popupElm = document.getElementById("fcitx5popup");
    if (popupElm) {
        return;
    }
    const popupHtml = '   <style>        #fcitx5popup {            position: fixed;            display: none;            background-color: white;            border: 2px solid #e0e0e0;            overflow: hidden;    z-index:9999999;  font-size:16px; font-family:sans;  }        #fcitx5popup.show {            display: block;        }        #preddit {            padding: 3px;  text-align:left;      }        #cursor {            user-select: none;        }        #cand {            display: flex;            cursor: pointer;            white-space: nowrap;        }        #cand>div {            padding: 4px 4px;        }        #cand>div:hover,        #cand:hover>div.selected:hover {            background-color: gray;            color: white;        }        #cand:hover .selected {            background-color: initial;            color: initial;        }        #cand .selected {            background-color: gray;            color: white;        }    </style>    <div id="fcitx5popup">        <div id="preddit">            <span></span><span id="cursor">|</span><span></span>        </div>        <div id="cand">        </div>    </div>';
    document.body.insertAdjacentHTML("afterbegin", popupHtml);
    setTimeout(()=>{
        window._fcitx5popup = document.getElementById("fcitx5popup");

        const ws = new WebSocket("ws://127.0.0.1:8088/server/fcitx5");
        let clear = true;
        let hasFocus = false;
        ws.addEventListener("message", async(e)=>{
            if (typeof e.data == 'string') {
                if (e.data.length < 5) {
                    clear = true;
                    window._fcitx5popup.classList.toggle('show', false);
                    return;
                }
            }
            /**@type {string} */
            const data = await e.data.text();
            clear = data.length < 5;
            if (clear) {
                window._fcitx5popup.classList.toggle('show', false);
                return;
            }
            const dataArr = data.split('\n');
            const curCursorPos = dataArr[1].indexOf('^');
            if (curCursorPos > -1) {

                const blob = new Blob([dataArr[0]]);
                const a = blob.slice(0, curCursorPos);
                const b = blob.slice(curCursorPos, blob.size);

                Promise.all([a.text(), b.text()])
                    .then(([s1, s2]) => {
                        window._fcitx5popup.children[0].children[0].textContent = s1;
                        window._fcitx5popup.children[0].children[2].textContent = s2;
                    });
            }
            const candList = dataArr.slice(2);
            const curCand = candList.findIndex(i=>i.startsWith('>'));
            let candHtml = '';
            candList.forEach((v,i)=>{
                if (i === curCand) {
                    candHtml += '<div class="selected">' + v.slice(1) + '</div>';
                } else {
                    candHtml += '<div>' + v + '</div>';
                }
            }
            );
            window._fcitx5popup.children[1].innerHTML = candHtml;
            if (hasFocus) {
                window._fcitx5popup.classList.toggle('show', true);
            }
        }
        );
        const focusHandler = (e)=>{
            showPopup(e.target);
        }
        ;

        const showPopup = (inputElm)=>{
            hasFocus = true;
            if (!clear) {
                window._fcitx5popup.classList.toggle('show', true);
            }
            const rect = inputElm.getBoundingClientRect();
            window._fcitx5popup.style.left = rect.x + 'px';
            window._fcitx5popup.style.top = rect.bottom + 'px';
        }
        ;

        const inputHandler = ()=>{
            hasFocus = true;
        }
        ;
        const blurHandler = (e)=>{
            const inputElm = e.target;

            hasFocus = false;
            window._fcitx5popup.classList.toggle('show', false);
        }
        ;

        const listen = (inputElm)=>{
            if (document.activeElement === inputElm) {
                showPopup(inputElm);
            }
            inputElm.addEventListener('focus', focusHandler);
            inputElm.addEventListener('blur', blurHandler);
            inputElm.addEventListener('input', inputHandler);
        }
        ;
        for (let inputElm of document.getElementsByTagName('input')) {
            listen(inputElm);
        }
        for (let inputElm of document.getElementsByTagName('textarea')) {
            listen(inputElm);
        }
    }
    , 0);
}
)();
