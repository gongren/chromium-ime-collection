
/**@type {HTMLInputElement} */
const inputElm = document.getElementById("ime-input");

const popupElm = document.getElementById("popup");

const ws = new WebSocket("ws://127.0.0.1:8088/server/fcitx5");

let clear = true;
let hasFocus = false;

ws.addEventListener("message", async (e) => {
    if (typeof e.data == 'string') {
        if (e.data.length < 5) {
            clear = true;
            popupElm.classList.toggle('show', false);

            return;
        }
    }
    /**@type {string} */
    const data = await e.data.text();

    clear = data.length == 0;
    if (clear) {
        popupElm.classList.toggle('show', false);
        return;
    }
    if (!popupElm.classList.contains('show')) {
        move(inputElm, popupElm);
    }
    const dataArr = data.split('\n');

    const curCursorPos = dataArr.length > 1 ? dataArr[1].indexOf('^') : -1;

    if (curCursorPos > -1) {

        const blob = new Blob([dataArr[0]]);
        const a = blob.slice(0, curCursorPos);
        const b = blob.slice(curCursorPos, blob.size);

        Promise.all([a.text(), b.text()])
            .then(([s1, s2]) => {
                popupElm.children[0].style.display = 'block';
                popupElm.children[0].children[0].textContent = s1;
                popupElm.children[0].children[2].textContent = s2;
                if (hasFocus) {
                    popupElm.classList.toggle('show', true);
                }
            })

        const candList = dataArr.slice(2);

        const curCand = candList.findIndex(i => i.startsWith('>'));

        let candHtml = ''
        candList.forEach((v, i) => {

            if (i === curCand) {
                candHtml += `<div class="selected">${v.slice(1)}</div>`
            } else {
                candHtml += `<div>${v}</div>`
            }

        })
        popupElm.children[1].innerHTML = candHtml;
    } else {
        popupElm.children[0].style.display = 'none';
        popupElm.children[1].innerHTML = dataArr.reduce((acc, i) => acc + i + '<br>', '');
        if (hasFocus) {
            popupElm.classList.toggle('show', true);
        }
    }
})

const prop = ['white-space', 'overflow-wrap', 'width', 'height', 'overflow', 'padding', 'border', 'font']
const cloneElm = document.createElement('div');
document.body.appendChild(cloneElm);

function getCursorPosition(elm) {
    cloneElm.style.position = 'fixed';
    cloneElm.style.left = '0px';
    cloneElm.style.top = '-1000px';


    const elmCSS = window.getComputedStyle(elm);

    for (const p of prop) {
        cloneElm.style[p] = elmCSS[p];
    }
    cloneElm.innerHTML = elm.value.slice(0, elm.selectionStart) + '<span></span>' + elm.value.slice(elm.selectionStart);
    cloneElm.scrollTop = elm.scrollTop;
    cloneElm.scrollLeft = elm.scrollLeft;

    const x = cloneElm.children[0].offsetLeft - cloneElm.scrollLeft;
    const y = cloneElm.children[0].offsetTop - cloneElm.scrollTop + cloneElm.children[0].offsetHeight;

    return { x, y };
}

function move(inputElm, popupElm) {
    const rect = inputElm.getBoundingClientRect();

    const { x: px, y: py } = getCursorPosition(inputElm);
    popupElm.style.left = rect.x + px + 'px'
    popupElm.style.top = rect.y + py + 'px';
    console.log(rect.x, rect.y, px, py)
}

inputElm.onfocus = () => {
    hasFocus = true;
    if (!clear) {
        popupElm.classList.toggle('show', true);
    }

    const rect = inputElm.getBoundingClientRect();
    setTimeout(() => {
        move(inputElm, popupElm);
    }, 0);
}
inputElm.onblur = () => {
    hasFocus = false;
    popupElm.classList.toggle('show', false);
}