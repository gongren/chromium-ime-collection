use std::collections::HashMap;
use std::env;
use std::io::Read;
use std::net::SocketAddr;
use std::sync::Arc;

use inotify_sys;

use futures_util::{SinkExt, StreamExt};
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::{Message, WebSocket};
use warp::Filter;

type Room = Arc<RwLock<HashMap<String, (mpsc::UnboundedSender<Message>, String)>>>;

fn ex(arr: &[u8]) -> [u8; 4] {
    if arr.len() == 4 {
        [arr[0], arr[1], arr[2], arr[3]]
    } else {
        [0, 0, 0, 0]
    }
}

fn add_watch(n_fd: i32) -> i32 {
    let mut w_fd = -1;

    let pathname = std::ffi::CString::new("/tmp/iimmee");
    if pathname.is_ok() {
        let pathname = pathname.unwrap();
        let name = pathname.as_ptr();
        unsafe {
            w_fd = inotify_sys::inotify_add_watch(n_fd, name, inotify_sys::IN_CLOSE_WRITE);
        }
        if w_fd == -1 {}
    }
    w_fd
}

#[tokio::main]
async fn main() {
    let room = Room::default();

    let room_c = room.clone();

    tokio::task::spawn_blocking(move || {
        let mut iimmee = Vec::with_capacity(2048);

        let mut n_fd;
        let mut w_fd = -1;

        let wait_duration = core::time::Duration::from_millis(100);

        unsafe {
            n_fd = inotify_sys::inotify_init();

            if n_fd != -1 {
                w_fd = add_watch(n_fd);
                if w_fd == -1 {
                    inotify_sys::close(n_fd);
                    n_fd = -1;
                }
            }
        }
        let mut event_buf = vec![0u8; 4096];
        'outer: loop {
            let buf_len;
            if n_fd != -1 && w_fd != -1 {
                unsafe {
                    let ptr = event_buf.as_mut_ptr() as *mut std::ffi::c_void;
                    buf_len = inotify_sys::read(n_fd, ptr, 4096);
                }

                if buf_len == 16 {
                    let mask = u32::from_ne_bytes(ex(&event_buf[4..8]));

                    if mask & inotify_sys::IN_IGNORED != 0 {
                        w_fd = add_watch(n_fd);
                    }
                } else if buf_len > 16 {
                    let mask = u32::from_ne_bytes(ex(&event_buf[20..24]));
                    if mask & inotify_sys::IN_IGNORED != 0 {
                        w_fd = add_watch(n_fd);
                    }
                }
            }
            if iimmee.len() > 0 {
                unsafe {
                    iimmee.set_len(0);
                }
            }

            std::thread::sleep(wait_duration);

            {
                let ime_file = std::fs::File::open("/tmp/iimmee");
                if ime_file.is_err() {
                    continue;
                }
                let mut ime_file = ime_file.unwrap();

                if ime_file.read_to_end(&mut iimmee).is_err() {
                    continue;
                }
            }

            {
                if iimmee.len() == 0 {
                    for (_, (tx, _)) in room_c.blocking_read().iter() {
                        if let Err(e) = tx.send(Message::text("")) {
                            eprintln!("{}: {}", line!(), e);
                            break 'outer;
                        }
                    }
                    continue;
                }
                for (_, (tx, _)) in room_c.blocking_read().iter() {
                    if let Err(e) = tx.send(Message::binary(iimmee.clone())) {
                        eprintln!("{}: {}", line!(), e);
                        break 'outer;
                    }
                }
            }
        }

        if n_fd != -1 {
            unsafe {
                inotify_sys::close(n_fd);
            }
        }
    });

    let room = warp::any().map(move || room.clone());

    let chat = warp::path!("server" / "fcitx5")
        .and(warp::ws())
        .and(room)
        .and(
            warp::header("x-forwarded-for")
                .or(warp::addr::remote().map(|addr: Option<SocketAddr>| addr.unwrap().to_string()))
                .unify(),
        )
        .map(|ws: warp::ws::Ws, room, addr| {
            ws.on_upgrade(move |socket| user_connected(socket, room, addr))
        });

    let addr = env::args().nth(1).unwrap_or("127.0.0.1:8088".to_owned());
    let path = env::args().nth(2).unwrap_or("./client".to_owned());

    eprintln!("http://{} {}", addr, path);

    let addr: SocketAddr = addr.parse().unwrap();

    let ass = warp::fs::dir(path);
    let routes = chat.or(ass);

    warp::serve(routes).run(addr).await;
}

async fn user_connected(ws: WebSocket, room: Room, addr: String) {
    eprintln!("new chat user: {}", addr);

    let (mut user_ws_tx, mut user_ws_rx) = ws.split();

    let (tx, rx) = mpsc::unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);

    tokio::task::spawn(async move {
        while let Some(message) = rx.next().await {
            if let Err(e) = user_ws_tx.send(message).await {
                eprintln!("{}#{}: {}", file!(), line!(), e);
                return;
            }
        }
    });

    room.write()
        .await
        .insert(addr.to_owned(), (tx, addr.clone()));

    while let Some(_) = user_ws_rx.next().await {}

    user_disconnected(&addr, &room).await;
}

async fn user_disconnected(my_id: &String, users: &Room) {
    eprintln!("good bye user: {}", my_id);

    users.write().await.remove(my_id);
}
