import os
from threading import Thread
from watchdog.observers.inotify import InotifyObserver
from watchdog.events import FileSystemEventHandler
from i3ipc import Connection, Event

os.environ['GTK_IM_MODULE'] = 'gtk-im-context-simple'

try:
    import gi
    gi.require_version('Gtk', '3.0')
    gi.require_version('GtkLayerShell', '0.1')
    from gi.repository import Gtk, GLib, GtkLayerShell
except Exception as e:
    raise e


class Handler(FileSystemEventHandler):
    def on_closed(self, event):

        s = ''
        xys = ''
        s_arr = []
        x = 0
        y = 0
        w = 0
        h = 0
        with open('/tmp/iimmeexxyy', mode='r') as f:
            xys = f.readline()

        xy = xys.split(' ')

        if len(xy) >= 4:
            x = int(xy[0])
            y = int(xy[1])
            w = int(xy[2])
            h = int(xy[3])
        else:
            return

        with open('/tmp/iimmee', mode='r', encoding='utf-8') as f:
            s_arr = f.readlines()

        if len(s_arr) > 1:

            try:
                cur_cursor = s_arr[1].index('^')

                byte_arr = s_arr[0].encode()

                cand = (byte_arr[0:cur_cursor] + b'|' +
                        byte_arr[cur_cursor:]).decode(encoding='utf-8', errors='ignore')
                j = 0
                for i in s_arr[2:]:
                    j += 1
                    if i.startswith('>'):
                        cand += '<span color="white" background="gray"> ' + \
                            str(j) + i[3:len(i)-1] + " </span>"
                    else:
                        cand += ' ' + str(j) + i[3:len(i)-1]+' '
                s = '<span line_height="1.3pt"> ' + cand + '</span>'
            except:
                s = ''.join(s_arr)
        else:
            s = ''.join(s_arr)

        if len(s) == 1:
            s = '<span line_height="1.3pt"> ' + s + ' </span>'

        global chromium_x, chromium_y
        x += chromium_x + w
        y += chromium_y + h
        GLib.idle_add(update_text, s, x, y)


chromium_x = 0
chromium_y = 0


def is_target(s: str):
    if s == 'chromium' or s == 'chrome' or s.startswith("chrome-"):
        return True
    else:
        return False


def on_event(i3, e):
    if is_target(e.container.app_id) and e.change != 'close':
        global chromium_x, chromium_y

        if e.container.fullscreen_mode == 1:
            chromium_x = chromium_y = 0
        else:
            chromium_x = e.container.rect.x
            chromium_y = e.container.rect.y


HIDE_POSITION = -100

win_x = HIDE_POSITION
win_y = HIDE_POSITION


def update_text(s, x, y):
    global win_x, win_y

    if win_x != x or win_y != y:
        if win_x < 0:
            window.hide()
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.LEFT, x)
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.TOP, y)
        if win_x < 0:
            window.show_all()
        win_x, win_y = x, y

    if len(s) > 0:
        label.set_markup(s)
    else:
        label.set_markup('')
        win_x = win_y = HIDE_POSITION
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.LEFT, win_x)
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.TOP, win_y)


provider = Gtk.CssProvider()
provider.load_from_data(
    b"#fcitx5popup{border:2px solid #c0c0c0;background-color:white;}")

window = Gtk.Window()

label = Gtk.Label()

label.set_use_markup(True)
label.set_name("fcitx5popup")
context = label.get_style_context()
context.add_provider(provider, 9999)

window.add(label)

observer = InotifyObserver()
observer.schedule(Handler(), "/tmp/iimmee")
observer.start()

i3 = Connection()

i3.on(Event.WINDOW, on_event)

th = Thread(target=lambda: i3.main())
th.daemon = True
th.start()

GtkLayerShell.init_for_window(window)
GtkLayerShell.set_layer(window, GtkLayerShell.Layer.OVERLAY)
# GtkLayerShell.auto_exclusive_zone_enable(window)
GtkLayerShell.set_margin(window, GtkLayerShell.Edge.TOP, HIDE_POSITION)
GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.TOP, 1)
GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.LEFT, 1)

window.show_all()
window.connect('destroy', Gtk.main_quit)
Gtk.main()
observer.stop()
