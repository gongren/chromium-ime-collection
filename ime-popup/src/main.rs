use inotify_sys;
use std::io::{Read, Write};
use std::{sync, thread};
use swayipc::{Connection, Fallible};

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Point {
    x: i32,
    y: i32,
}

fn add_watch(n_fd: i32) -> i32 {
    let mut w_fd = -1;

    let pathname = std::ffi::CString::new("/tmp/iimmee");
    if pathname.is_ok() {
        let pathname = pathname.unwrap();
        let name = pathname.as_ptr();
        unsafe {
            w_fd = inotify_sys::inotify_add_watch(n_fd, name, inotify_sys::IN_CLOSE_WRITE);
        }
        if w_fd == -1 {}
    }
    w_fd
}

fn is_target(s: &str) -> bool {
    if s.eq("chromium") || s.eq("chrome") || s.starts_with("chrome-") {
        return true;
    }

    false
}

fn ex(arr: &[u8]) -> [u8; 4] {
    if arr.len() == 4 {
        [arr[0], arr[1], arr[2], arr[3]]
    } else {
        [0, 0, 0, 0]
    }
}

const OUT_SCREEN_X: i32 = -2000;

fn main() -> Fallible<()> {
    let mut connection = Connection::new()?;

    let wait_duration = core::time::Duration::from_millis(100);

    let mut old_point = Point { x: 0, y: 0 };
    let mut new_point = Point { x: 0, y: 0 };

    let app_id = std::env::var("P_APP_ID").unwrap_or("_fcitx5_sway".to_owned());
    let default_size = std::env::var("P_APP_SIZE").unwrap_or("151 220".to_owned());

    let default_size_int: Vec<i32> = default_size
        .split(" ")
        .map(|i| i.parse::<i32>().unwrap())
        .collect();
    if default_size_int.len() != 2 || default_size_int[0] < 1 || default_size_int[1] < 1 {
        panic!("P_APP_SIZE must have two positive numbers.");
    }
    let c_point = sync::Arc::new(sync::RwLock::new(Point { x: 0, y: 0 }));
    let current_app = sync::Arc::new(sync::RwLock::new(String::new()));

    let back_point = c_point.clone();

    let app_id_c = app_id.clone();
    let current_app_c = current_app.clone();

    let screen_geo = match connection.get_outputs() {
        Ok(outputs) => Point {
            x: outputs[0].rect.width,
            y: outputs[0].rect.height,
        },
        _ => Point { x: 1280, y: 720 },
    };
    thread::spawn(move || {
        let connection = Connection::new();

        if connection.is_err() {
            return;
        }
        let mut connection = connection.unwrap();

        thread::sleep(core::time::Duration::from_millis(2000));

        connection.run_command(format!(
            "[app_id=\"{}\"] move position -1000 -1000",
            app_id_c
        ));
        connection.run_command(format!("[app_id=\"{}\"] border pixel", app_id_c));
        connection.run_command(format!(
            "[app_id=\"{}\"] resize set {}",
            app_id_c, default_size
        ));

        let es = connection.subscribe([swayipc::EventType::Window]).unwrap();

        for e in es {
            if e.is_err() {
                continue;
            }
            let e = e.unwrap();
            match e {
                swayipc::Event::Window(box_e) => match box_e.change {
                    swayipc::WindowChange::Move => {
                        if let Some(app_id) = &box_e.container.app_id {
                            if !is_target(app_id) {
                                continue;
                            }

                            let chromium_node = box_e.container;
                            if let Ok(mut p) = back_point.write() {
                                p.x = chromium_node.rect.x;
                                p.y = chromium_node.rect.y + chromium_node.deco_rect.height;
                            }
                        }
                    }
                    swayipc::WindowChange::Focus => {
                        if let Some(app_id) = &box_e.container.app_id {
                            if let Ok(mut a) = current_app_c.write() {
                                a.clear();
                                a.push_str(&app_id);

                                if !is_target(app_id) {
                                    continue;
                                }

                                let chromium_node = box_e.container;
                                if let Ok(mut p) = back_point.write() {
                                    p.x = chromium_node.rect.x;
                                    p.y = chromium_node.rect.y + chromium_node.deco_rect.height;
                                }
                            }
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }
    });

    let mut last_time = std::time::SystemTime::now();
    let mut iimmee = Vec::with_capacity(2048);
    let mut xxyy = Vec::with_capacity(16);

    let mut n_fd;
    let mut w_fd = -1;

    unsafe {
        n_fd = inotify_sys::inotify_init();

        if n_fd != -1 {
            w_fd = add_watch(n_fd);
            if w_fd == -1 {
                inotify_sys::close(n_fd);
                n_fd = -1;
            }
        }
    }
    let mut event_buf = vec![0u8; 4096];
    loop {
        let buf_len;
        if n_fd != -1 && w_fd != -1 {
            unsafe {
                let ptr = event_buf.as_mut_ptr() as *mut std::ffi::c_void;
                buf_len = inotify_sys::read(n_fd, ptr, 4096);
            }

            if buf_len == 16 {
                let wd = i32::from_ne_bytes(ex(&event_buf[0..4]));
                let mask = u32::from_ne_bytes(ex(&event_buf[4..8]));
                let cookie = u32::from_ne_bytes(ex(&event_buf[8..12]));
                let len = u32::from_ne_bytes(ex(&event_buf[12..16]));
                if mask & inotify_sys::IN_IGNORED != 0 {
                    w_fd = add_watch(n_fd);
                }
            } else if buf_len > 16 {
                let wd = i32::from_ne_bytes(ex(&event_buf[16..20]));
                let mask = u32::from_ne_bytes(ex(&event_buf[20..24]));
                let cookie = u32::from_ne_bytes(ex(&event_buf[24..28]));
                let len = u32::from_ne_bytes(ex(&event_buf[28..32]));
                if mask & inotify_sys::IN_IGNORED != 0 {
                    w_fd = add_watch(n_fd);
                }
            }
        }
        if iimmee.len() > 0 {
            unsafe {
                iimmee.set_len(0);
            }
        }
        if xxyy.len() > 0 {
            unsafe {
                xxyy.set_len(0);
            }
        }

        thread::sleep(wait_duration);
        if let Ok(a) = current_app.read() {
            if !is_target(&a) && a.ne(&app_id) {
                continue;
            }
        }
        let ime_size;
        {
            let ime_stat = std::fs::metadata("/tmp/iimmee");
            if ime_stat.is_err() {
                continue;
            }

            let ime_stat = ime_stat.unwrap();

            if let Ok(t) = ime_stat.modified() {
                if t == last_time {
                    continue;
                } else {
                    last_time = t;
                }
            }

            ime_size = ime_stat.len();
        }
        if ime_size < 5 {
            if old_point.x == OUT_SCREEN_X {
                continue;
            }
            new_point.x = OUT_SCREEN_X;
            new_point.y = 10;
        } else {
            {
                let ime_file = std::fs::File::open("/tmp/iimmee");
                if ime_file.is_err() {
                    continue;
                }
                let mut ime_file = ime_file.unwrap();

                if ime_file.read_to_end(&mut iimmee).is_err() {
                    continue;
                }
            }

            {
                let out = std::io::stdout();
                let mut handle = out.lock();

                handle.write(b"\x1B[2J\x1B[3J\x1B[H");
                if iimmee.len() == 0 {
                    handle.flush();
                    continue;
                }
                iimmee.pop();
                handle.write(&iimmee);
                handle.flush();
            }

            {
                let xxyy_file = std::fs::File::open("/tmp/iimmeexxyy");
                if xxyy_file.is_err() {
                    continue;
                }
                let mut xxyy_file = xxyy_file.unwrap();

                if xxyy_file.read_to_end(&mut xxyy).is_err() {
                    continue;
                }
            }

            let imeposi = String::from_utf8_lossy(&xxyy);
            let pop_psi: Vec<i32> = imeposi
                .split(" ")
                .map(|s| s.parse::<i32>().unwrap_or_default())
                .collect();

            if pop_psi.len() < 4 {
                continue;
            }
            if let Ok(p) = c_point.read() {
                new_point.x = p.x + pop_psi[0] + pop_psi[2];
                new_point.y = p.y + pop_psi[1] + pop_psi[3];
            }
        }
        if old_point != new_point {
            if new_point.x + default_size_int[0] + 10 > screen_geo.x {
                new_point.x -= default_size_int[0] + 10;
            }
            if new_point.y + default_size_int[1] + 10 > screen_geo.y {
                new_point.y -= default_size_int[1] + 10;
            }
            connection.run_command(format!(
                "[app_id=\"{}\"] move position {} {}",
                app_id, new_point.x, new_point.y
            ));
            //eprintln!("{:?} {:?}", ret, new_point);
            old_point = new_point
        }
    }
}
